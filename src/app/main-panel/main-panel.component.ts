import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.scss']
})
export class MainPanelComponent implements OnInit {
  showFanQuote: boolean=false;
  showLockQuote: boolean = false;
  showAshTrayQuote: boolean = false;
  showPurseQuote: boolean = false;
  showHatQuote: boolean = false;
  showWatchQuote: boolean = false;
  showBagQuote: boolean = false;
  showTelephoneQuote: boolean = false;
  showBeltQuote: boolean = false;
  showBulletQuote: boolean = false;
  showCandleQuote: boolean = false;
  showGunQuote: boolean = false;
  showTelescopeQuote: boolean = false;
  showAvacadoQuote: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showQuoteForFan(object){
    if(this.showFanQuote){
      this.showFanQuote = false;
    }else{
      this.showFanQuote = true;
    }
    console.log(object);
  }

  showQuoteForLock(object){
    if(this.showLockQuote){
      this.showLockQuote = false;
    }else{
      this.showLockQuote=true;
    }
    console.log(object);
  }

  showQuoteForAshTray(object){
    if(this.showAshTrayQuote){
      this.showAshTrayQuote = false;
    }else{
      this.showAshTrayQuote=true;
    }
    console.log(object);
  }

  showQuoteForPurse(object){
    if(this.showPurseQuote){
      this.showPurseQuote = false;
    }else{
      this.showPurseQuote = true;
    }
    console.log(object);
  }

  showQuoteForHat(object){
    if(this.showHatQuote){
      this.showHatQuote = false;
    }else{
      this.showHatQuote = true;
    }
    console.log(object);
  }

  showQuoteForWatch(object){
    if(this.showWatchQuote){
      this.showWatchQuote = false;
    }else{
      this.showWatchQuote = true;
    }
    console.log(object);
  }

  showQuoteForBag(object){
    if(this.showBagQuote){
      this.showBagQuote = false;
    }else{
      this.showBagQuote = true;
    }
    console.log(object);
  }

  showQuoteForTelephone(object){
    if(this.showTelephoneQuote){
      this.showTelephoneQuote = false;
    }else{
      this.showTelephoneQuote = true;
    }
    console.log(object);
  }

  showQuoteForBelt(object){
    if(this.showBeltQuote){
      this.showBeltQuote = false;
    }else{
      this.showBeltQuote = true;
    }
    console.log(object);
  }

  showQuoteForBullet(object){
    if(this.showBulletQuote){
      this.showBulletQuote = false;
    }else{
      this.showBulletQuote = true;
    }
    console.log(object);
  }

  showQuoteForCandle(object){
    if(this.showCandleQuote){
      this.showCandleQuote = false;
    }else{
      this.showCandleQuote = true;
    }
    console.log(object);
  }

  showQuoteForGun(object){
    if(this.showGunQuote){
      this.showGunQuote = false;
    }else{
      this.showGunQuote = true;
    }
    console.log(object);
  }

  showQuoteForTelescope(object){
    if(this.showTelescopeQuote){
      this.showTelescopeQuote = false;
    }else{
      this.showTelescopeQuote = true;
    }
    console.log(object);
  }

  showQuoteForAvacado(object){
    if(this.showAvacadoQuote){
      this.showAvacadoQuote = false;
    }else{
      this.showAvacadoQuote = true;
    }
    console.log(object);
  }

}
